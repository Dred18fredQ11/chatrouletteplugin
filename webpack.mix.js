const mix = require('laravel-mix');
var WebpackObfuscator = require('webpack-obfuscator');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.options({
    uglify: {
        compress: false,
    }
});

mix.webpackConfig({
    plugins: [
        new WebpackObfuscator ({
            rotateStringArray: true
        })
    ]
});

mix
    .js('bg.js', 'build/bg.js')
    .js('injected.js', 'build/injected.js')
    .copy('manifest.json', 'build/')
    .css('injected.css', 'build/injected.css')
