let iframe = document.getElementById('videochat');

if(iframe !== null) {
    let iframeWindow = iframe.contentWindow;

    iframe.addEventListener('load', (event)=> {
        iframeWindow.postMessage({location: window.location.href}, '*');
    });
}

