console.log('injected.js loaded.')

window.addEventListener('load', (event) => {
    // modifyInterface();
})

window.addEventListener("message", (event) => {
    console.log(event.data);

    if (event.data.location !== undefined) {
        catchPopups();
        modifyInterface();
    }
}, false);

let stream, videoChannel,
    videoAllowed = false,
    isStarted = false,
    gifs = [
    'https://aimodify.com/img/0.gif',
        'https://aimodify.com/img/1.gif',
        'https://aimodify.com/img/2.gif',
        'https://aimodify.com/img/3.gif',
        'https://aimodify.com/img/4.gif'
    ];

function cloneMediaStream(targetContainerId = 'remoteVideo2', sourceElementId = 'local-video') {
    let video = document.getElementById(sourceElementId) // remote-video
    let captured = document.getElementById(targetContainerId)

    captured.srcObject = null;
    stream = video.captureStream()
    captured.srcObject = stream
}

function modifyInterface() {
    chatTemplate();
    cloneMediaStream('localVideo', 'local-video');

    document.getElementById('remote-video').onplaying = function (e) {
        cloneMediaStream('remoteVideo', 'remote-video');

        if (videoAllowed) {
            cloneMediaStream('remoteVideo2', 'remoteVideo');
        }
    }

    document.getElementById('allow-video').onclick = function (e) {
        e.target.innerHTML = (videoAllowed ? 'Enable' : 'Disable') + ' video';
        allowStreamVideo();
    }

    document.getElementById('start-chat').onclick = function () {
        triggerButtonEvent('start-button');
        // cloneMediaStream('remoteVideo', 'remote-video')
        isStarted = true
        changeButtonsState()
    }

    document.getElementById('stop-chat').onclick = function () {
        triggerButtonEvent('stop-button');
        isStarted = false
        changeButtonsState()
    }

    document.getElementById('next-chat').onclick = function () {
        triggerButtonEvent('start-button');
        // cloneMediaStream('remoteVideo', 'remote-video')
        isStarted = true
        changeButtonsState()
    }
}


function takeScreenshot() {

    const track = stream.getVideoTracks()[0];
    videoChannel = new ImageCapture(track);

    videoChannel.grabFrame()
        .then(imageBitmap => {
            const canvas = document.getElementById('stream-image');
            drawCanvas(canvas, imageBitmap);
        })
        .catch(error => ChromeSamples.log(error));

    // check frame/photo performance differences
    // function onTakePhotoButtonClick() {
    //     imageCapture.takePhoto()
    //         .then(blob => createImageBitmap(blob))
    //         .then(imageBitmap => {
    //             const canvas = document.querySelector('#takePhotoCanvas');
    //             drawCanvas(canvas, imageBitmap);
    //         })
    //         .catch(error => ChromeSamples.log(error));
    // }

}

function drawCanvas(canvas, img) {
    canvas.width = getComputedStyle(canvas).width.split('px')[0];
    canvas.height = getComputedStyle(canvas).height.split('px')[0];
    let ratio = Math.min(canvas.width / img.width, canvas.height / img.height);
    let x = (canvas.width - img.width * ratio) / 2;
    let y = (canvas.height - img.height * ratio) / 2;
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
    canvas.getContext('2d').drawImage(img, 0, 0, img.width, img.height,
        x, y, img.width * ratio, img.height * ratio);
}

function allowStreamVideo() {
    let captured = document.getElementById('remoteVideo2');

    if (videoAllowed) {
        this.innerText = 'Allow video'
        captured.srcObject = null;
        captured.setAttribute("poster", gifs[Math.floor(Math.random() * gifs.length)]);
        videoAllowed = false;
    } else {
        this.innerText = 'Disable video'
        cloneMediaStream('remoteVideo2', 'remoteVideo')
        captured.removeAttribute("poster");
        videoAllowed = true;
    }
}

function changeButtonsState() {
    if (isStarted) {
        document.getElementById('start-chat').setAttribute('disabled', true)
        document.getElementById('next-chat').removeAttribute('disabled')
        document.getElementById('stop-chat').removeAttribute('disabled')
    } else {
        document.getElementById('start-chat').removeAttribute('disabled')
        document.getElementById('next-chat').setAttribute('disabled', true)
        document.getElementById('stop-chat').setAttribute('disabled', true)
    }
}

function chatTemplate() {
    const tpl = `
    <div id="ai-tpl-container">
        <div class="controls">
            <button type="button" class='' id="allow-video">Enable video</button>
            <button type="button" class='' id="start-chat">Start chat</button>
            <button type="button" disabled class='' id="next-chat">Next chat</button>
            <button type="button" disabled class='' id="stop-chat">Stop chat</button>
        </div>
        <video id="localVideo" autoplay muted></video>
        <video id="remoteVideo" autoplay muted></video>
        <video id="remoteVideo2" autoplay muted></video>
        <div class='chat'>
            <div class='chat-messages'>
            </div>
            <div class='chat-input'>
                <div method='post' id='chat-form'>
                    <input type='text' id='message-text' class='chat-form__input' placeholder='enter message'>
                    <button type="submit" class='chat-form__submit' id="chat_send">=></button>
                </div>
            </div>
        </div>
    </div>
    `

    document.getElementsByTagName('body')[0].insertAdjacentHTML('beforeend', tpl);

}

function triggerButtonEvent(className) {
    const event = new MouseEvent('click', {view: window, bubbles: true, cancelable: true})
    const element = document.getElementsByClassName(className)[0];
    element.dispatchEvent(event);
}

function catchPopups() {

    document.querySelector('#local-video-warning-popup .ok').onclick = function (e) {
        document.getElementById('ai-tpl-container').style.visibility = 'visible';
    }

    let trackChanges = function (elemId) {

        console.log('bound to ' + elemId);

        let MutationObserver = window.WebKitMutationObserver;

        let target = document.getElementById(elemId);

        let observer = new MutationObserver(function (mutations) {
            mutations.forEach(function (mutation) {
                // console.log('old', mutation.oldValue);
                // console.log('new', mutation.target.style.display);
                // console.log(mutation.target.style.display);

                if (mutation.target.style.display === 'none') {
                    // console.log(document.getElementById('ai-tpl-container').style.display);
                    document.getElementById('ai-tpl-container').style.visibility = 'visible';
                    // console.log(document.getElementById('ai-tpl-container').style.display);
                } else {
                    document.getElementById('ai-tpl-container').style.visibility = 'hidden';
                }

            });
        });

        const config = {
            attributeOldValue: true,
            attributes: true,
            childList: false,
            characterData: false
        }

        observer.observe(target, config);
    }

    const trackItems = [
        'local-video-warning-popup',
        // 'report-popup',
        // 'disconnection-popup',
        // 'ban-popup',
        // 'app-popup',
        // 'settings-popup'
    ];

    trackItems.forEach(elemId => trackChanges(elemId))

}